package Account;

public class Account {
    private double balance;
    private String name;
    private static double interestRate = 1.1;

    public static double getInterestRate() {
		return Account.interestRate;
	}

    public static void setInterestRate(double interestRate) {
		Account.interestRate = interestRate;
	}

    public double getBalance() {
        return this.balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addInterest() {
        balance*=interestRate;
    }

    public Account(String s, double d) {
        name = s;
        balance = d;
    }

    public Account() {
        this("Michael", 50);
    }

    public boolean withdraw(double amount) {
        boolean flag = false;
        if ((balance-amount)>0) {
			balance -= amount;
            flag = true;
            System.out.println("You have withdrawn £" + amount + ". Your balance is now £" + balance + " " + name + ".");
        } 
        else
            System.out.println("Your balance is £" + balance + ". You don't have enough " + name + ".");
        return flag;
        }
    }
