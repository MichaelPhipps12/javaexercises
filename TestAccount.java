package Account;

public class TestAccount {
    public static void main(String[] args) {
        Account myAccount = new Account();
        // Creates new insance of class Account called myAccount
        myAccount.setBalance(1000);
        // Sets balance using setBalance method
        myAccount.setName("Michael Phipps");
        // Sets name using setName method
        myAccount.addInterest();
        // Calls addInterest method on myAccount
        System.out.println("Name is " + myAccount.getName());
        // Prints name
        System.out.println("Balance is " + "£" + String.format("%.2f", myAccount.getBalance()));
        // Displays balance with two decimal figures

        Account[] arrayOfAccounts = new Account[5];
        //Creates array of size 5
        double[] amounts = { 23, 5444, 2, 345, 34 };
        String[] names = { "Picard", "Ryker", "Worf", "Troy", "Data" };
        for(int i=0; i<=4; i++){
            arrayOfAccounts[i] = new Account(); //THIS IS THE LINE THAT I WAS MISSING!
            arrayOfAccounts[i].setBalance(amounts[i]);
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].addInterest();
            System.out.println("Name is " + arrayOfAccounts[i].getName());
            System.out.println("Balance is " + "£" + String.format("%.2f", arrayOfAccounts[i].getBalance()));
       }
    }
}