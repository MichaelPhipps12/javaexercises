package Account;

public class TestAccount2 {
    public static void main(String[] args) {
        Account.setInterestRate(1.5);
        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = {23,5444,2,345,34};
		String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};
        for(int i = 0; i<=4; i++) {
            arrayOfAccounts[i] = new Account(names[i], amounts[i]);
            System.out.println(arrayOfAccounts[i].getName() + " has got £" + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("With interest, " + arrayOfAccounts[i].getName() + "s balance is £" + arrayOfAccounts[i].getBalance());
        }
        arrayOfAccounts[3].withdraw(100);
    }
    
}
